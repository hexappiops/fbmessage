#!/usr/bin/env node

const electron = require('electron')
const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')

let win
let width = 800
let height = 600

function createWindow() {
    if (process.platform == 'darwin') {
        icon = __dirname + 'img/icon_darwin.icns'
    } else if (process.platform == 'windows') {
        icon = __dirname + 'img/icon_windows.ico'
    } else {
        icon = __dirname + 'img/icon_posix.png'
    }
    opts = {
        width: width,
        height: height,
        icon: icon,
        fullscreenable: true,
        title: 'Messenger',
        webPreferences: {
            nodeIntegration: false,
            devTools: true,
            webSecurity: false
        }
    }
    win = new BrowserWindow(opts)
    win.loadURL('https://messenger.com')
    //win.webContents.openDevTools() // open devtools (debug)

    win.on('closed', () => {
        // remember last size
        size = win.getSize()
        width = size[0]
        height = size[1]
        win = null

    })
}

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (win === null) {
        createWindow()
    }
})
